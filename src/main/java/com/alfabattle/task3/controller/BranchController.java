package com.alfabattle.task3.controller;

import com.alfabattle.task3.entity.BranchesEntity;
import com.alfabattle.task3.repositories.BranchesRepo;
import com.alfabattle.task3.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin("*")
@RestController
@RequestMapping("/branches")
@Slf4j
public class BranchController {

    @Autowired
    BranchesRepo branchesRepo;

    @GetMapping("{branchId}")
    public BranchesEntity getBranch(@PathVariable Integer branchId) {
        List<BranchesEntity> all = branchesRepo.findAll();
        log.info(all.toString());

        List<BranchesEntity> collect = all.stream()
                .filter(branchesEntity -> branchId.equals(branchesEntity.getId()))
                .collect(Collectors.toList());

        if (collect.isEmpty()) {
            throw new NotFoundException("branch not found");
        }

        log.info(collect.toString());

        return collect.get(0);
    }
}
