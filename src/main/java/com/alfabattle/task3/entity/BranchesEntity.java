package com.alfabattle.task3.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@ToString
@Table(name = "branches", schema = "public")
public class BranchesEntity
{
    @Id
    private int id;
    private String title;
    private Double lon;
    private Double lat;
    private String address;
}

