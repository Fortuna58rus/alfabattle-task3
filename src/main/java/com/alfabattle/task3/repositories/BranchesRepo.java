package com.alfabattle.task3.repositories;

import com.alfabattle.task3.entity.BranchesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BranchesRepo extends JpaRepository<BranchesEntity, Integer> {
    List<BranchesEntity> findAll();
}
